//
//  PushupViewController.swift
//  BodyPOC
//
//  Created by Rafeeq CE on 22/07/16.
//  Copyright © 2016 Mobiefit. All rights reserved.
//

import UIKit

class PushupViewController: UIViewController {
    let notificationCenter = NSNotificationCenter.defaultCenter()
    let device = UIDevice.currentDevice()
    
    var repCounter = 0
    
    @IBOutlet weak var repCountLbl: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupProximitySensor()
    }
    
    func setupProximitySensor()
    {
        device.proximityMonitoringEnabled = true
        notificationCenter.addObserver(self, selector: #selector(PushupViewController.proximityChanged(_:)), name: UIDeviceProximityStateDidChangeNotification, object: device)

    }
    
    func proximityChanged(sender: UIDevice)
    {
        let state = device.proximityState
        
        if (state)
        {
            repCounter += 1
            
            repCountLbl.text = String(repCounter)
        }
    }
    
}
