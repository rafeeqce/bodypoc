//
//  SquatViewController.swift
//  BodyPOC
//
//  Created by Rafeeq CE on 27/07/16.
//  Copyright © 2016 Mobiefit. All rights reserved.
//

import UIKit

class SquatViewController: UIViewController {
    
    var currentDeviceOrientation : UIDeviceOrientation = .Unknown
    let motionKit = MotionKit()
    @IBOutlet weak var repCountLbl: UILabel!
    
    var repCounter =  0 {
        didSet {
            dispatch_async(dispatch_get_main_queue()) {
                self.repCountLbl.text = String(self.repCounter)
            }
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        UIDevice.currentDevice().beginGeneratingDeviceOrientationNotifications()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(SquatViewController.deviceDidRotate), name: UIDeviceOrientationDidChangeNotification, object: nil)
        
        // Initial device orientation
        self.currentDeviceOrientation = UIDevice.currentDevice().orientation
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        motionKit.getAccelerometerValues(1.0) { (x, y, z) -> () in
            if (UIDeviceOrientationIsPortrait(self.currentDeviceOrientation))
            {
                if (abs(y) > 1)
                {
                    self.repCounter += 1
                }
            }
            else if (UIDeviceOrientationIsLandscape(self.currentDeviceOrientation))
            {
                if (abs(x) > 1)
                {
                    self.repCounter += 1
                }
            }
        }
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        NSNotificationCenter.defaultCenter().removeObserver(self)
        if UIDevice.currentDevice().generatesDeviceOrientationNotifications {
            UIDevice.currentDevice().endGeneratingDeviceOrientationNotifications()
        }
    }
    
    func deviceDidRotate()
    {
        currentDeviceOrientation = UIDevice.currentDevice().orientation
    }
}
